﻿using System;
using System.IO;
using System.Linq;

namespace AdventOfCode
{
	class Program
	{
		static void Main(string[] args)
		{
			var ints = File.ReadAllLines("input.txt")
				.Select(s => Int32.Parse(s)).ToList();
			int changes = 0;
			for (int i = 1; i < ints.Count; i++)
			{
				if (ints[i - 1] < ints[i])
				{
					changes++;
				}
			}
			Console.WriteLine(changes);

			changes = 0;
			for (int i = 3; i < ints.Count; i++)
			{
				changes += ints[i] > ints[i - 3] ? 1 : 0;
			}

			Console.WriteLine(changes);
			Console.ReadKey();
		}
	}
}
